
% Read the picture of the Eiffel tower, and convert to black and white.

E_Tower = rgb2gray(imread('Eiffel.jpg'));

% Downsample, just to avoid dealing with high-resolution images.
%E_Tower = im2double(imresize(E_Tower, 0.5));% scale image by factor 0.5

E_Tower = E_Tower'; % ' : transpose operation

E_Tower = im2double(E_Tower);
% Make reduced row Echelon form:

[R,p] = rref(E_Tower);

% p is a vector contains the indices of the pivots

%Make matrix M by using only the basic columns of "Eiffel"-matrix

M = E_Tower(:,p);

% Show full-rank picture of Eiffel tower
figure; 
imshow(E_Tower'), title('Original Eiffel tower picture');


% Show compressed Eiffel tower image via using its basic columns
figure
    
imshow(M'), title('Compressed Eiffel tower image');

