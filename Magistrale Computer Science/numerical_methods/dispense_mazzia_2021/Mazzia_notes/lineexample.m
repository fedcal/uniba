% Example of ill conditioned system
A  =[.835  .667; .333 .266];
b  = [.168;.067];
ba = [.168;.066];

x1=A\b
x2=A\ba



x = linspace(-700,100,1e5);
l1y = (-.835*x+.168)/.667;
l2y = (-.333*x+.067)/.266;
l2ya = (-.333*x+.066)/.266;



figure(1)
plot(x, l1y,'r.',x,l2y,'g.',x,l2ya,'b.')


figure(2)
plot(x,l1y-l2ya,'r.',x,l1y-l2y,'g.',x,zeros(size(x)),'b')