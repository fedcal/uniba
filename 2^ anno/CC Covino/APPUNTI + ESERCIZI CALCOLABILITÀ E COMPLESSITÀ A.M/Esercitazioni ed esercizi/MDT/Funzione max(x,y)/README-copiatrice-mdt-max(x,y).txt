La mdT per la funzione f(x,y)= max(x,y) deve innanzitutto implementare una copiatrice, la quale prende in input due numeri in notazione unaria, con 0 come separatore dei due numeri e 00 come separatore per la copia dell'input.

La copiatrice parte sull'input inserito (del tipo: 1111011100)
Quello che fa la macchina è:

ripeti
- legge un 1 a destra e lo marca con # (membro x)
- scavalca tutti gli altri 1 a destra ("")
- scavalca lo 0 separatore
- scavalca tutti gli 1 del membro y
- scavalca i due 00
- se legge degli 1 o degli 0 li scavalca, altrimenti se legge un blank inserisce 1
- scavalca tutti gli 1 e 0 che legge a sinistra
- legge un # a sinistra e si sposta a destra
fino a quando non legge 0.

Se legge uno 0 allora si è finito di copiare il membro x

- legge 0 e lo lascia come tale
- legge tutti gli 0 e tutti gli uno fino a quando non incontra un blank
- legge il blank e inserisce uno 0
- si sposta a sinistra e legge di nuovo tutti gli 0 e 1 fino a quando non incontra un #
- legge un # e si sposta a destra
- legge uno 0 e si sposta a destra

ripeti
- legge un 1 e lo sostituisce a #
- legge tutti gli 1 e 0 fino a quando non trova un blank
- inserisce un 1 e si sposta a sinistra, leggendo tutti gli 1 e 0 che trova
- legge un # e si sposta a destra
fino a quando non trova più 1 da sostituire

La macchina si ferma, con la testina puntata sul primo simbolo della copia.
