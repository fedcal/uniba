Visto il funzionamento della copiatrice, adesso si deve pensare alla funzione vera e propria. L'input da inserire nella mdt per funzionare correttamente dev'essere nella forma seguente: x0y, a differenza dell'input per la copiatrice (che era x0y00).

La macchina molto semplicemente fa dei confronti a destra e sinistra, anche qui segnando come fa solitamente una copiatrice. In base al momento in cui i simboli sono finiti a destra oppure a sinistra, la macchina si adegua e manda in output uno dei due risultati: x è max | y è max, con la testina posizionata sul primo simbolo del risultato.

Anche qui, la notazione utilizzata per segnare è quella che sfrutta il simbolo "#".

