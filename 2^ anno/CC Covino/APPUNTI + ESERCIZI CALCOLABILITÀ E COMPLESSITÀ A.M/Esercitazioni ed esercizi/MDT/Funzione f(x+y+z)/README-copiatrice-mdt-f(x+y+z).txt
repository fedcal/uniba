La mdT per la funzione f(x+y+z) deve innanzitutto implementare una copiatrice, la quale prende in input tre numeri in notazione unaria, con 0 come separatore dei tre numeri e 00 come separatore per la copia dell'input.

La copiatrice parte sull'input inserito (del tipo: 111101110111100)
Quello che fa la macchina è:

ripeti
- legge un 1 a destra e lo marca con # (membro x)
- scavalca tutti gli altri 1 a destra 
- scavalca lo 0 separatore
- scavalca tutti gli 1 e tutti gli 0 che incontra, spostandosi a destra
- legge un blank, scrive 1 e si sposta a sinistra
- legge tutti gli 0 ed 1 a sinistra fino a quando non incontra un #
- legge #, lo lascia come tale e si sposta a destra
fino a quando non legge un 0

Se legge uno 0 allora si è finito di copiare il membro x.

- legge 0 e lo lascia come tale, spostandosi a sinistra
- tutti gli # che legge li trasforma in 1 a sinistra, fino a quando non trova un blank
- legge il blank e si sposta a destra
- legge tutti gli 1 e tutti gli 0 a destra fino a quando non trova un blank
- legge un blank, ci inserisce uno 0 e si sposta a sinistra
- legge tutti gli 1 a sinistra fino a quando non incontra i due 0 separatori
- legge gli 0 separatori spostandosi a sinistra
- legge nuovamente tutti gli 1 a sinistra fino a quando non incontra uno 0
- legge uno 0, lo lascia come tale e si sposta a destra

Qui s'inizia a copiare il secondo membro (membro y)

ripeti
- legge un 1 a destra e lo marca con #
- scavalca tutti gli altri 1 a destra
- scavalca lo 0 separatore
- scavalca tutti gli 1 e tutti gli 0 che incontra, spostandosi a destra
- legge un blank, scrive 1 e si sposta a sinistra
- legge tutti gli 0 ed 1 a sinistra fino a quando non incontra un #
- legge #, lo lascia come tale e si sposta a destra
fino a quando non legge un 0


Se legge uno 0 allora si è finito di copiare il membro y.

- legge 0 e lo lascia come tale, spostandosi a sinistra
- tutti gli # che legge li trasforma in 1 a sinistra, fino a quando non trova uno 0
- legge lo 0 e si sposta a destra
- legge tutti gli 1 e tutti gli 0 a destra fino a quando non trova un blank
- legge un blank, ci inserisce uno 0 e si sposta a sinistra
- legge tutti gli 1 a sinistra fino a quando non incontra uno 0 separatore
- legge lo 0 separatore spostandosi a sinistra
- legge di nuovo tutti gli 1 a sinistra fino a quando non incontra i due 0 separatori
- legge i due zeri separatori spostandosi a sinistra
- legge per l'ultima volta tutti gli 1 a sinistra fino a quando non incontra uno 0
- legge uno 0, lo lascia come tale e si sposta a destra

Qui s'inizia a copiare il terzo membro (membro z)

ripeti
- legge un 1 a destra e lo marca con #
- scavalca tutti gli 1 e gli 0 a destra fino a quando non trova un blank
- legge un blank, lo trasforma in 1 e si sposta a sinistra
- legge tutti gli 1 e gli 0 a sinistra fino a quando non trova un #
- legge un #, lo lascia come tale e si sposta a destra
fino a quando non legge un 0

Se legge uno 0 allora si è finito di copiare il membro z.

- legge 0, lo lascia come tale e si sposta a sinistra
- tutti gli # li trasforma in 1, spostandosi a sinistra fino a quando non in contra uno 0
- legge 0, lo lascia come tale e si sposta a destra
- legge tutti gli 1 a destra fino a quando non incontra i due 0 separatori
- legge i due zero separatori, spostandosi a destra e posizionando la testina sul primo simbolo della copia.


Quando è finito il membro y, la testina si trova sullo 0 e si sposta a sinistra per poter trasformare tutti gli # in 1. Per finire, la macchina riposiziona la propria testina sul primo simbolo della stringa copiata.

