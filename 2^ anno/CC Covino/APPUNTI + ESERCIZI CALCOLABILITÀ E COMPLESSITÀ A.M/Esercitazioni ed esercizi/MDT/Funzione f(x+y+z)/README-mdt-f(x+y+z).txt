Visto il funzionamento della copiatrice, adesso si deve pensare alla funzione vera e propria. L'input da inserire nella mdt per funzionare correttamente dev'essere nella forma seguente: x0y0z, a differenza dell'input per la copiatrice (che era x0y0z00).

La macchina molto semplicemente esegue questi passaggi:
- legge tutti gli 1 a destra
- legge uno 0 che trasforma in 1, spostandosi a destra
- legge di nuovo tutti gli altri 1 a destra
- legge uno 0 che trasforma nuovamente in 1, spostandosi ancora a destra
- legge tutti gli 1 a destra fino a quando non trova un blank
- legge un blank, lo lascia come tale e si sposta a sinistra
- legge quattro simboli 1 che trasforma in blank, spostandosi a sinistra.

Così facendo si ottiene un algoritmo che dà precisamente come risultato la funzione f(x+y+z).


