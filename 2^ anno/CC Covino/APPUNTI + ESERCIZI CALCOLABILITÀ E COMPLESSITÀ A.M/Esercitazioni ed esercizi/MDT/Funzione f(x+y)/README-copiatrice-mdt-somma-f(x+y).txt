La mdT per la funzione f(x+y) deve innanzitutto implementare una copiatrice, la quale prende in input due numeri in notazione unaria, con 0 come separatore dei due numeri e 00 come separatore per la copia dell'input.

La copiatrice parte sull'input inserito (del tipo: 1111011100)
Quello che fa la macchina è:

ripeti
- legge un 1 a destra e lo marca con # (membro x)
- scavalca tutti gli altri 1 a destra ("")
- scavalca lo 0 separatore
- scavalca tutti gli 1 del membro y
- scavalca i due 00
- se legge degli 1 li scavalca a destra, altrimenti se legge un blank inserisce 1 e si sposta a sinistra
- scavalca tutti gli 1 e 0 che legge a sinistra
- legge un # a sinistra e si sposta a destra
fino a quando non legge 0.

Se legge uno 0 allora si è finito di copiare il membro x.

- legge 0 e lo lascia come tale, spostandosi a sinistra
- tutti gli # che legge li trasforma in 1 a sinistra, fino a quando non trova un blank
- legge il blank e si sposta a destra
- legge tutti gli 1 fino a quando non trova uno 0
- legge uno 0, lo lascia come tale e si sposta a destra
- legge tutti gli 1 a destra (membro y)
- legge i due 0 separatori e tutti gli 1 del membro x appena copiato
- legge un blank, ci inserisce lo 0 e si sposta a sinistra
- legge di nuovo tutti gli 1 a sinistra
- legge di nuovo i due separatori 0
- legge gli 1 del membro y fino a quando non trova lo 0, che permette alla testina di posizionarsi sul primo simbolo 1 del membro y

ripeti
- legge 1 a destra e lo marca con # (membro y)
- legge tutti gli 1 e 0 successivi a destra
- legge un blank, scrive 1 e si sposta a sinistra
- legge tutti gli 1 a sinistra fino a quando non trova uno 0
- legge uno 0 e si sposta a sinistra
- legge tutti gli 1 e 0 a sinistra fino a quando non trova un #
- legge un # che lascia come tale e si sposta a destra
fino a quando non è finito il membro y.

Quando è finito il membro y, la testina si trova sullo 0 e si sposta a sinistra per poter trasformare tutti gli # in 1. Per finire, la macchina riposiziona la propria testina sul primo simbolo della stringa copiata.

