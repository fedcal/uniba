Visto il funzionamento della copiatrice, adesso si deve pensare alla funzione vera e propria. L'input da inserire nella mdt per funzionare correttamente dev'essere nella forma seguente: x0y, a differenza dell'input per la copiatrice (che era x0y00).

La macchina molto semplicemente legge tutti gli 1, trasforma lo 0 in 1, legge tutti gli 1 a destra fino a quando non trova un blank.
Legge il blank, si posiziona a sinistra ed elimina gli ultimi due simboli 1. Così, molto semplicemente, si è ottenuta la funzione di somma.

