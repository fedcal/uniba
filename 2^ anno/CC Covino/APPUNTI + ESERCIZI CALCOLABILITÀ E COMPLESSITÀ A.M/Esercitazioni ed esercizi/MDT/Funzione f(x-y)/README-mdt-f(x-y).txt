Visto il funzionamento della copiatrice, adesso si deve pensare alla funzione vera e propria. L'input da inserire nella mdt per funzionare correttamente dev'essere nella forma seguente: x0y, a differenza dell'input per la copiatrice (che era x0y00).

La macchina molto semplicemente legge tutti gli 1 e si sposta a destra fino a quando non trova uno 0, che lascia come tale e si sposta a destra.
Legge nuovamente tutti gli 1 e si sposta a destra fino a quando non trova un blank, che lascia come tale, e si sposta a sinistra.
Tutti gli 1 che legge a sinistra li trasforma in blank, fino a quando non trova lo 0 separatore. Anche lo 0 viene trasformato in blank, spostandosi a sinistra.
Elimina ulteriormente un singolo 1 a sinistra, trasformandolo in blank. Per finire, legge tutti gli 1 a sinistra e posiziona la propria testina sul primo simbolo del risultato ottenuto.
Questo algoritmo è molto semplice e permette di ottenere la funzione f(x-y).

