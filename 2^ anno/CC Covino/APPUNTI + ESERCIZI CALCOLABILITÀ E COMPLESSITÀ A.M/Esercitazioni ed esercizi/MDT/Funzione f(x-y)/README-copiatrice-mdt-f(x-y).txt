La mdT per la funzione f(x-y) deve innanzitutto implementare una copiatrice, la quale prende in input due numeri in notazione unaria, con 0 come separatore dei due numeri e 00 come separatore per la copia dell'input.

La copiatrice parte sull'input inserito (del tipo: 1111011100)
Quello che fa la macchina è:

ripeti
- legge un 1 a destra e lo marca con # (membro x)
- scavalca tutti gli altri 1 a destra ("")
- scavalca lo 0 separatore
- scavalca tutti gli 1 e 0 a destra fino a quando non legge un blank
- legge un blank, sostituisce con 1 e si sposta a sinistra
- legge tutti gli 0 ed 1 a sinistra fino a quando non trova un simbolo #
- legge un #, lo lascia come tale e si sposta a destra
fino a quando non trova uno 0

Se legge uno 0 allora si è finito di copiare il membro x.

- legge 0 e lo lascia come tale, spostandosi a sinistra
- tutti gli # che legge li trasforma in 1 a sinistra, fino a quando non trova un blank
- legge il blank e si sposta a destra
- legge tutti gli 1 e 0 a destra fino a quando non trova un blank
- legge il blank, lo sostituisce con 0 e si sposta a sinistra
- legge tutti gli 1 a sinistra fino a quando non trova i due 0 separatori
- legge gli 0 separatori e si sposta a sinistra
- legge tutti gli 1 a sinistra fino a quando non trova uno 0
- legge lo 0 e si sposta a destra

ripeti
- legge un 1, lo sostituisce con # e si sposta a destra
- legge tutti gli 1 e 0 a destra fino a quando non trova un blank
- legge il blank, lo sostituisce con 1 e si sposta a sinistra
- legge nuovamente tutti gli 1 e 0 a sinistra fino a quando non trova un #
- legge un #, lo lascia come tale e si sposta a destra
fino a quando non trova uno 0

- legge lo 0, lo lascia come tale e si sposta a sinistra
- tutti gli # li trasforma in 1, spostandosi a sinistra fino a quando non trova uno 0
- legge uno 0, lo lascia come tale e si sposta a destra
- legge tutti gli 1 a destra

La macchina legge per finire i due 0 separatori, e posiziona la sua testina sull'input appena finito di copiare.

