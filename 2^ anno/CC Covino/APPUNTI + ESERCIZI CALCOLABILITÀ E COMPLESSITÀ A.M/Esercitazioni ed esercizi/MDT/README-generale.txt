Ciao!
Questi sono gli esercizi per le mdt che ho fatto e che hanno risultati corretti. Ne ho fatti degli altri, ma alcuni erano incompleti e altri troppo inutilmente complessi. Spero che questi bastino.
Mi sono anche molto esercitata sulle copiatrici (pensavo fossero essenziali per l'esame, ma in realtà dipende dalla richiesta del professore).
Oltre alla directory "Copiatrici", per ogni esercizio giusto per fare pratica ho fatto sempre copiatrici. Hanno quasi tutte un'esecuzione simile, ma potrai ben notare le differenze.
Per le funzioni in sè, quando ti eserciterai tu stesso, dovrai pensarci da solo. Le soluzioni verranno solo sforzandoti e facendo prove (a me ha molto aiutato fare prima prove su carta).
Detto questo, buono studio :)
