La copiatrice permette di copiare un input del tipo seguente: a^(n)0, dove a^(n) è il numero di "a" che si vogliono inserire come input e 0 è il simbolo separatore. Input diversi da questo tipo non sono accettati. Con il meccanismo nella macchina si segnano le a di partenza con un simbolo qualsiasi (#), in modo da avere un conto.

A differenza della copiatrice a distanza 1, questa copiatrice segna due simboli come # anziché uno solo. Inizialmente la copiatrice è stata pensata per avere in input solo stringhe di lunghezza pari, accettando comunque stringhe di lunghezza dispari ma sbagliando la copia. Ho aggiunto una variante modificata e completa, dove gli input possono essere anche dispari.
Una condizione: l'input dev'essere almeno di lunghezza 2. 

Anche qui si eseguono dei cicli, nei quali si segnano due a, ci si sposta dopo lo 0, e si scrivono due a.
Il ciclo si ripete fino a quando non sono finite le a da copiare. Finita la copia, si riscrivono i simboli # in a.
Chiaramente, ci sono gli opportuni controlli su ciò che si legge sul nastro della macchina di turing e su ciò che dev'essere scavalcato a destra o sinistra.
