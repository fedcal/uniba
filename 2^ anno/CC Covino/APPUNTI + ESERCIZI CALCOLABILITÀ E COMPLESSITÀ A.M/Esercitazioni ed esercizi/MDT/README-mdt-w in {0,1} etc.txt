La mdT per il linguaggio L={w in {0,1}* | il n di 0 in w è = n di 1} è una funzione che, perlomeno nel mio caso, è stata molto intricata da realizzare - seppur complessa, fà il suo dovere.

La macchina può leggere input sull'alfabeto {0,1}* di qualsiasi lunghezza. L'input è accettato solo se soddisfa la condizione del linguaggio.

Scrivere i passaggi uno per volta sarebbe troppo pesante mentalmente da leggere, come lo è in realtà la macchina stessa. Però posso spiegarti cosa fa in breve la mia soluzione :)

Innanzitutto, la mia macchina delimita la stringa inserita con il simbolo "@", uno a sinistra e quindi prima dell'inizio della stringa, ed uno a destra alla fine della stringa. 
Fatto questo, la macchina inizia a segnare i simboli dell'alfabeto che incontra sulla stringa: se incontra un 1, sostituisce all'1 una X; se incontra uno 0, sostituisce allo 0 una Y.
Non appena viene fatta la sostituzione, se il simbolo sostituito è un 1 la macchina porta a destra quell'uno (ovvero aggiunge dietro il simbolo @ di destra il simbolo 1), altrimenti se il simbolo sostituito è un 0 la macchina porta a sinistra quello 0 (dietro @ a sinistra).

Quando i simboli sono finiti, ci si trova ad esempio con una stringa del genere (supponendo che la stringa rispetti le condizioni):
0000@XYXYXYYX@1111

Qui la macchina inizia a contare i simboli da un lato e dall'altro. Toglie ad uno ad uno i simboli, a sinistra e destra. Nel caso in cui la macchina trovi più simboli 1 o 0 da uno dei due estremi, allora la stringa non rispetta le condizioni e non fa parte del linguaggio. 
Quando invece, come nell'esempio sopra, la stringa appartiene al linguaggio tutti gli 1 e 0 dietro i @ vengono rimossi e le X ed Y ritornano ad essere la stringa iniziale.

Spero l'algoritmo e la spiegazione ti siano stati chiari!
Chiaramente ti consiglio di provare tu stesso con qualche stringa su JFLAP, magari prova con l'opzione Input > Step.

